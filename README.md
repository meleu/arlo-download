# Arlo Download

The project takes advantage of [Jeffrey Dwalter](https://github.com/jeffreydwalter/arlo)'s python library and example and creats a self contained docker image.

All videos in basic Arlo account from 7 days ago will be downloaded. By default, videos are organized by date and stored in the directory where the docker command is executed.

---

## How To

To run the image from command line:
(Use the same credential for [Arlo Web Portal](https://my.arlo.com/))

1. From an empty directory
   ```
   cd <an empty directory>
   ```
1. Put user/password in a file, e.g. `.env`
   ```
   ARLO_USER=<arlo login>
   ARLO_PASSWORD=<arlo password>
   ```
1. Run docker container in daemon mode
   ```
   docker run -it -d \
     -v ${PWD}:/arlo/videos \
     --env-file=.env \
     meleu/arlo-download
   ```

Available variables:

- `ARLO_USER`: (required) Specify arlo login name.
- `ARLO_PASSWORD`: (required) Specify arlo login password.
- `TZ`: (default UTC) Specify timezone, see available TZ names [here](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones).
- `USE_SCHEDULE`: (default 1) Use scheduler to download videos. By default, container will run in the background, and download videos during desired [schedule](src/cronjob). To download video at ad hoc, set it to `0`.
- `KEEP_DAYS`: (default 90) Number of days to keep the videos, anything older is deleted.

---

## Schedule

It is important to set the proper timezone when working with the schedule. E.g. set `TZ="Asia/Taipei"` in `.env`. If not set, timezone defaults to UTC.

By default two jobs are scehduled to run daily:

1. Download, runs at 2am everyday
1. Cleanup (delete files older than 90 days), runs at 3am everyday

To change or disable any schedule job, save a copy and modify [cronjob](src/cronjob) locally, e.g. new_cronjob. Override original schedule with `-v ${PWD}/new_cronjob:/arlo/cronjob`. Full command:

```
docker run -it -d \
  -v ${PWD}:/arlo/videos \
  -v ${PWD}/new_cronjob:/arlo/ \
  --env-file=.env \
  meleu/arlo-download
```

---

# Credit

[Jefffrey Dwalter's python library](https://github.com/jeffreydwalter/arlo)

[Jefffrey Dwalter's golang library](https://github.com/jeffreydwalter/arlo-go)
