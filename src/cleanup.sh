#!/bin/sh

# number of days to keep (default: 90 days)
[ -z ${KEEP_DAYS} ] && KEEP_DAYS=90

# Last day to keep, anything older, delete
LASTDAY=$(($(date '+%s') - ${KEEP_DAYS}*24*60*60))

echo "Deleting anything older than ${KEEP_DAYS}days from `date`"
for i in `ls videos`
do
    COMPARE=$(date -d "${i} 00:00:00" +%s)
    if [ "${COMPARE}" -le "${LASTDAY}" ]; then
        echo "deleting ${i} . . ."
        rm -rf videos/${i}
    fi
done