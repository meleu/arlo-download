#!/bin/sh

# make sure variables are defined
[ -z ${TZ} ] && TZ="Etc/UTC"
[ -z ${USE_SCHEDULE} ] && USE_SCHEDULE=1

if [ ${USE_SCHEDULE} == 1 ]
then
    # setup TZ for crontab
    ln -sf /usr/share/zoneinfo/${TZ} /etc/localtime
    cat /arlo/cronjob >> /etc/crontabs/root

    crond start

    # keep the container running
    tail -f /dev/null
    exit
else
    cd /arlo
    python /arlo/arlo-download.py
fi
