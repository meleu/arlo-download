FROM python:alpine

LABEL org.opencontainers.image.title="arlo-download"
LABEL org.opencontainers.image.source="https://gitlab.com/meleu/arlo-download"
LABEL org.opencontainers.image.authors="Eric Leu"

#setting default values
ENV ARLO_USER=nobody
ENV ARLO_PASSWORD=mypassword
ENV TZ="Etc/UTC"
ENV USE_SCHEDULE=1
ENV KEEP_DAYS=90

WORKDIR /arlo

COPY src/ ./

RUN apk update && apk --no-cache add \
    git \
    tzdata \
    && pip install git+https://github.com/jeffreydwalter/arlo

CMD ["/bin/sh","/arlo/entrypoint.sh"]